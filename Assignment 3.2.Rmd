---
title: "Assignment 3 - Part 2 - Diagnosing Schizophrenia from Voice"
author: "Sara Kolding"
date: "November 1, 2018"
output: word_document
---

https://gitlab.com/sarakolding/experimentalmethods3
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#loading libraries
library(pacman, tidyverse)
p_load(ggplot2, lme4, stringr, dplyr, reshape2, growthcurver, purrr, modelr, Metrics, caret, simr, stats, FinCal, nonlinearTseries, scales, GMCM, pROC)

#setting working directory
setwd("C:/Users/Sara/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/THIRD SEMESTER/EXPERIMENTAL METHODS 3/CLASSES/Experimental Methods 3/experimentalmethods3")

#loading data
PitchData <- read.csv("PitchData.csv")
PitchData$Subject = as.numeric(as.factor(PitchData$Subject))
```

## Assignment 3 - Diagnosing schizophrenia from voice

In the previous part of the assignment you generated a bunch of "features", that is, of quantitative descriptors of voice in schizophrenia, focusing on pitch.
In the course of this assignment we will use them to try to automatically diagnose schizophrenia from voice only, that is, relying on the set of features you produced last time, we will try to produce an automated classifier.

### Question 1: Can you diagnose schizophrenia from pitch range only? If so, how well?

Build a logistic regression to see whether you can diagnose schizophrenia from pitch range only.

```{r, include = FALSE}
PitchData$diagnosis <- as.numeric(PitchData$diagnosis)

SimpleRangeModel <- glm(diagnosis ~ range, PitchData, family = "binomial")
summary(SimpleRangeModel)
```

Range is a significant predictor for diagnosis (p < 0.05).

Calculate the different performance measures (accuracy, sensitivity, specificity, PPV, NPV, ROC curve) on a logistic regression using the full dataset. Don't forget the random effects!

```{r, echo = FALSE, warning = FALSE}
for (i in 9:17){ 
  PitchData[,i] = scales::rescale(PitchData[,i], to=c(0,1))
}

PitchData$trial <- as.numeric(PitchData$trial)

RangeModel <- glmer(diagnosis ~ range + (1+trial|Subject) + (1|study), PitchData, family = "binomial")
#summary(RangeModel)

#ACCURACY
PitchData$PredictionsPerc = GMCM:::inv.logit(predict(RangeModel))
PitchData$Predictions[PitchData$PredictionsPerc > 0.5] = "1"
PitchData$Predictions[PitchData$PredictionsPerc <= 0.5] = "0"
#confusionMatrix(data = PitchData$Predictions, reference = PitchData$diagnosis, positive = "1") 

#(326+469)/(326+338+185+490)
#0.5937267

#SENSITIVITY
PitchData$Predictions <- as.factor(PitchData$Predictions)
PitchData$diagnosis <- as.factor(PitchData$diagnosis)

#sensitivity(data = PitchData$Predictions, reference = PitchData$diagnosis, positive = "1")
#0.7259259

#SPECIFICITY
#specificity(data = PitchData$Predictions, reference = PitchData$diagnosis, negative = "0") 
#0.4909639

#PPV
#posPredValue(data = PitchData$Predictions, reference = PitchData$diagnosis, positive = "1")
#0.5917874

#NPV
#negPredValue(data = PitchData$Predictions, reference = PitchData$diagnosis, negative = "0") 
#0.6379648

#ROC CURVE
RangeRoc <- roc(response = PitchData$diagnosis, predictor = PitchData$PredictionsPerc)
#auc(RangeRoc) #0.6774
#ci(RangeRoc) #95% CI: 0.6491-0.7057
plot(RangeRoc, legacy.axes = TRUE) 

#changing the prevalence manually
#posPredValue(data = Data$Predictions, reference = Data$diagnosis, positive = "ASD", prevalence = .59) 
```

Performance measures; 
Accuracy 0.594
Sensitivity 0.726
Specificity 0.491
PPV 0.592
NPV 0.638

Then cross-validate the logistic regression and re-calculate performance on the testing folds. N.B. The cross-validation functions you already have should be tweaked: you need to calculate these new performance measures.

```{r, echo = FALSE, warning = FALSE}
set.seed(4)
folds <- createFolds(unique(PitchData$Subject), 10)
PitchData$Subject <- as.numeric(as.factor(PitchData$Subject))

#RANGE
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ range + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

RangeCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

N.B. the predict() function generates log odds (the full scale between minus and plus infinity). Log odds > 0 indicates a choice of 1, below a choice of 0.
N.B. you need to decide whether calculate performance on each single test fold or save all the prediction for test folds in one dataset, so to calculate overall performance.
N.B. Now you have two levels of structure: subject and study. Should this impact your cross-validation?

Performance measures; 
Accuracy 0.547
Sensitivity 0.708
Specificity 0.384
PPV 0.539
NPV 0.564

### Question 2 - Which single acoustic predictor is the best predictor of diagnosis?

```{r, include = FALSE}
#MEAN
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ mean + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

MeanCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#SD
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ sd + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

SDCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#MIN
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ min + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

MinCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#MAX
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ max + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

MaxCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#MEDIAN
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ median + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

MedianCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#IQR
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ iqr + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

IQRCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#MAD
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ mad + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

MADCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, include = FALSE}
#COEFFICIENT VARIANCE
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ coefvar + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

CoefVarCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")
```

```{r, echo = FALSE, warning = FALSE}
PerfMeasuresAll <- rbind(RangeCM$byClass, MeanCM$byClass, MinCM$byClass, MaxCM$byClass, MedianCM$byClass, SDCM$byClass, IQRCM$byClass, MedianCM$byClass)

PerfMeasuresAll <- PerfMeasuresAll[, c(11, 1, 2, 3, 4)]

rownames(PerfMeasuresAll) <- c("Range", "Mean", "Minimum", "Maximum", "Median", "Standard Deviation", "Interquartile Range", "Median Absolute Deviation")
colnames(PerfMeasuresAll) <- c("Accuracy", "Sensitivity", "Specificity", "PPV", "NPV")
PerfMeasuresAll
```

We argue that interquartile range of pitch is the best predictor for our priorities; we want to falsely exclude as few as possible (false negative). Since interquartile range has both the highest sensitivity and accuracy, we deem that to be the best predictor. The model with coefficient variance as a predictor failed to converge.

### Question 3 - Which combination of acoustic predictors is best for diagnosing schizophrenia?

Now it's time to go wild! Use all (voice-related) variables and interactions you can think of. Compare models and select the best performing model you can find.

Remember:
- Out-of-sample error crucial to build the best model!
- After choosing the model, send Malte and Riccardo the code of your model

```{r, eval = FALSE, include = FALSE}
for (i in 1:length(folds)){
  f <- folds[[i]]
  train = filter(PitchData,!(Subject %in% f))
  test = filter(PitchData,(Subject %in% f))
  model = glmer(diagnosis ~ iqr + (1+trial|Subject) + (1|study), train, family="binomial")
  PitchData$pred[PitchData$Subject %in% f] = GMCM:::inv.logit(predict(model, test, allow.new.levels = TRUE))
  PitchData$preddiag[PitchData$pred>0.5]="1"
  PitchData$preddiag[PitchData$pred<=0.5]="0"
  PitchData$preddiag <- as.factor(PitchData$preddiag)
  temp_df <- data_frame(diagnosis = PitchData$diagnosis,pred = PitchData$pred,preddiag = PitchData$preddiag, fold_nr = i)
  if (i == 1){
    result_df <- temp_df
  } else {
    result_df <- rbind(result_df, temp_df)
  }
}

FavCM <- confusionMatrix(data = PitchData$preddiag, reference = PitchData$diagnosis, positive = "1")

model = glmer(diagnosis ~ iqr + (1+trial|Subject) + (1|study), PitchData, family="binomial")
summary(model)
```

Since more complex models either failed to converge or elicited higher out-of-sample error than simpler models, our best performing model remains the interquartile range of pitch model with the structure; diagnosis ~ interquartile range of pitch + (1+trial|subject) + (1|study). The effect of interquartile range of pitch is significant (p < 0.05). The performance measures calculated by 10-folds cross-validation were; accuracy = 0.5795, sensitivity = 0.8504, specificity = 0.3042, PPV = 0.5541, NPV = 0.6667.

### Question 4: Properly report the results

METHODS SECTION: how did you analyse the data? That is, how did you extract the data, designed the models and compared their performance?

RESULTS SECTION: can you diagnose schizophrenia based on voice? which features are used? Comment on the difference between the different performance measures.

The variables used in the models were acoustic features extracted per participant per trial from pitch. The best performing model was selected based on performance measures calculated by 10-folds cross-validation. 
We used R (R Core Team, 2012) and lme4 (Bates, Maechler & Bolker, 2012) to perform a linear mixed effects analysis of the relationship between interquartile range of pitch and diagnosis, in individuals diagnosed with scizophrenia and matched controls. As fixed effects, we entered interquartile range of pitch into the model. As random effects, we had an intercept for subjects, as well as a by‐subject random slope for the effect of trial, and an intercept for study. P‐values were obtained by use of the LmerTest package (Kuznetsova, Brockhoff & Christensen, 2017). The effect of interquartile range of pitch as a predictor was significant (p < 0.05). However, performance measures obtained suggested that the accuracy of this predictor was not much higher than change (0.5795). Sensitivity (that is true positives/all positives, or how many true patients we are correctly including) was 0.8504, specificity (that is true negatives/all negatives, or how many controls were are correctly excluding) was 0.3042, positive predictive value (PPV - the probability of having scizophrenia if you are diagnoised) was 0.5541 and the negative predictive value (NPV - the probability of not having schizophrenia if you are not diagnosis) was 0.6667. We decided that the most important measure was sensitivity, since our priority was to falsely exclude as few as possible (false negative).
