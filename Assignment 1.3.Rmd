---
title: "Assignment 1 - Language Development in ASD - part 3"
author: "Sara Kolding"
date: "October 3, 2018"
output: word_document
---

https://gitlab.com/sarakolding/experimentalmethods3

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Welcome to the third exciting part of the Language Development in ASD exercise

In this exercise we will delve more in depth with different practices of model comparison and model selection, by first evaluating your models from last time, then learning how to cross-validate models and finally how to systematically compare models.

N.B. There are several datasets for this exercise, so pay attention to which one you are using!

1. The (training) dataset from last time (the awesome one you produced :-) ).
2. The (test) datasets on which you can test the models from last time:
* Demographic and clinical data: https://www.dropbox.com/s/ra99bdvm6fzay3g/demo_test.csv?dl=1
* Utterance Length data: https://www.dropbox.com/s/uxtqqzl18nwxowq/LU_test.csv?dl=1
* Word data: https://www.dropbox.com/s/1ces4hv8kh0stov/token_test.csv?dl=1

```{r setup and data cleaning, include = FALSE}
#loading libraries
library(pacman, tidyverse)
p_load(ggplot2, lme4, stringr, dplyr, reshape2, growthcurver, purrr, modelr, Metrics, caret)

#setting working directory
setwd("C:/Users/Sara/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/THIRD SEMESTER/EXPERIMENTAL METHODS 3/CLASSES/Experimental Methods 3/experimentalmethods3")

#loading data
demo <- read.csv("demo_test.csv")
LU <- read.csv("LU_test.csv")
token <- read.csv("token_test.csv")

#renaming columns
colnames(demo)[1] <- "SUBJ"
colnames(demo)[2] <- "VISIT"

#extracting digits
LU$VISIT <- str_extract(LU$VISIT, "\\d")
token$VISIT <- str_extract(token$VISIT, "\\d")

#removing all punctuations
LU$SUBJ <- str_replace_all(LU$SUBJ, "[[:punct:]]", "")
token$SUBJ <- str_replace_all(token$SUBJ, "[[:punct:]]", "")
demo$SUBJ <- str_replace_all(demo$SUBJ, "[[:punct:]]", "")

#selecting relevant columns
demoselect <- select(demo, SUBJ, VISIT, Ethnicity, Diagnosis, Gender, Age, ADOS,  MullenRaw, ExpressiveLangRaw)
LUselect <- select(LU, SUBJ, VISIT, MOT_MLU, MOT_LUstd, CHI_MLU, CHI_LUstd)
tokenselect <- select(token, SUBJ, VISIT, types_MOT, types_CHI, tokens_MOT, tokens_CHI)

#merging dataframes
LUselect$VISIT <- as.numeric(LUselect$VISIT)
demoLU <- full_join(demoselect, LUselect)

tokenselect$VISIT <- as.numeric(tokenselect$VISIT)
test <- full_join(demoLU, tokenselect)

#renaming and replacing columns
colnames(test)[8] <- "nonverbalIQ"
colnames(test)[9] <- "verbalIQ"

testsub <- subset(test, VISIT == 1, select = c(SUBJ, ADOS, nonverbalIQ, verbalIQ))
testsel <- filter(test, VISIT == "1") %>% select(SUBJ, ADOS, nonverbalIQ, verbalIQ)

colnames(testsub)[2] <- "ADOS1"
colnames(testsub)[3] <- "nonverbalIQ1"
colnames(testsub)[4] <- "verbalIQ1"

#adding new columns
test1 <- full_join(test, testsub)
test1 <- subset(test1, select = -c(ADOS, nonverbalIQ, verbalIQ))

#subsetting
test2 <- test1[, c(1, 4, 6, 5, 3, 2, 15, 16, 17, 7:14)]

#changing labels
test2$Gender <- ifelse(test2$Gender == "1", "M", "F")
test2$Diagnosis <- ifelse(test2$Diagnosis == "A", "ASD", "TD")
```

### Exercise 1) Testing model performance

How did your models from last time perform? In this exercise you have to compare the results on the training data () and on the test data. Report both of them. Compare them. Discuss why they are different.

- recreate the models you chose last time (just write the model code again and apply it to your training data (from the first assignment))
- calculate performance of the model on the training data: root mean square error is a good measure. (Tip: google the function rmse())
- create the test dataset (apply the code from assignment 1 part 1 to clean up the 3 test datasets)
- test the performance of the models on the test data (Tips: google the functions "predict()")
- optional: predictions are never certain, can you identify the uncertainty of the predictions? (e.g. google predictinterval())

formatting tip: If you write code in this document and plan to hand it in, remember to put include=FALSE in the code chunks before handing in.

```{r, include = FALSE}
#TRAINING
train <- read.csv("C:/Users/Sara/OneDrive - Aarhus universitet/COGNITIVE SCIENCE/THIRD SEMESTER/EXPERIMENTAL METHODS 3/CLASSES/Experimental Methods 3/LanguageDevelopmentData.csv")

growthquad <- lmer(CHI_MLU ~ Diagnosis + VISIT + I(VISIT^2) + (1+VISIT+I(VISIT^2)|SUBJ), train, REML = "FALSE")
summary(growthquad)

intercept <- lmer(CHI_MLU ~ 1 + VISIT + I(VISIT^2) + (1+VISIT+I(VISIT^2)|SUBJ), train, REML = "FALSE")
summary(intercept)

FavModel <- lmer(CHI_MLU ~ Diagnosis * (VISIT + I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT+I(VISIT^2)|SUBJ), train, REML = FALSE)
summary(FavModel)

anova(intercept, growthquad, FavModel)

#performance
Metrics::rmse(train$CHI_MLU[!is.na(train$CHI_MLU)], predict(FavModel)) #average error for predictions is 0.291

#TEST
#performance
Metrics::rmse(test2$CHI_MLU[!is.na(test2$CHI_MLU)], predict(FavModel, test2[!is.na(test2$CHI_MLU), ], allow.new.levels = TRUE)) #average error for predictions is 0.53
```

We fitted a quadratic model with the structure; child MLU ~ diagnosis * (visit + visit^2) + ADOS + verbal IQ + (visit + visit^2|subject). The average error of predictions for children's MLU is higher for the test data (MSE = 0.531) than the training data (MSE = 0.291), because the model is trained on the training data, and probably also models some of the idiosyncratic variance of the training data.

### Exercise 2) Model Selection via Cross-validation (N.B: ChildMLU!)

One way to reduce bad surprises when testing a model on new data is to train the model via cross-validation. 

In this exercise you have to use cross-validation to calculate the predictive error of your models and use this predictive error to select the best possible model.

- Use cross-validation to compare your model from last week with the basic model (Child MLU as a function of Time and Diagnosis, and don't forget the random effects!)
- (Tips): google the function "createFolds";  loop through each fold, train both models on the other folds and test them on the fold)

Which model is better at predicting new data: the one you selected last week or the one chosen via cross-validation this week?

- Test both of them on the test data.
- Report the results and comment on them.

- Now try to find the best possible predictive model of ChildMLU, that is, the one that produces the best cross-validated results.

- Bonus Question 1: What is the effect of changing the number of folds? Can you plot RMSE as a function of number of folds?
- Bonus Question 2: compare the cross-validated predictive error against the actual predictive error on the test data

```{r, include = FALSE}
#SIMPLE MODEL
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ 1 + VISIT + I(VISIT^2) + (1+VISIT+I(VISIT^2)|SUBJ), trainset, REML = "FALSE")
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.29
mean(MSEtest) #0.84

#OLD MODEL
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ Diagnosis + VISIT + I(VISIT^2) + (1+VISIT+I(VISIT^2)|SUBJ), trainset, REML = "FALSE")
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.29
mean(MSEtest) #0.83

#KIRI
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ VISIT + Diagnosis + verbalIQ1 + Gender + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ)+ (1|Ethnicity), trainset, REML = FALSE)
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.28
mean(MSEtest) #0.66

#SIGNE 1 - p-hacking
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ VISIT + Diagnosis + ADOS1 + verbalIQ1 + nonverbalIQ1 + MOT_MLU + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ) + (1|Ethnicity), trainset, REML = FALSE)
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.28
mean(MSEtest) #0.63

#SIGNE 2 - cross-validation
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ VISIT + Diagnosis + MOT_MLU +ADOS1 + types_CHI + tokens_CHI + I(VISIT^2)+(1+VISIT+ I(VISIT^2)|SUBJ), trainset, REML = FALSE)
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.271
mean(MSEtest) #0.483

#NEW
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ Diagnosis * (VISIT + I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT+I(VISIT^2)|SUBJ), trainset, REML = FALSE)
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.293
mean(MSEtest) #0.581

#NEW new
set.seed = 1
folds <- createFolds(unique(train$SUBJ), 5)
MSEtrain = NULL
MSEtest = NULL
n = 1

for (f in folds){
  trainset = subset(train, !(SUBJ %in% f))
  testset = subset(train, (SUBJ %in% f))
  modelquad <- lmer(CHI_MLU ~ Diagnosis * (VISIT +I(VISIT^2)) * verbalIQ1 + ADOS1 + (1+VISIT+ I(VISIT^2)|SUBJ), trainset, REML = FALSE)
  MSEtrain[n] <- rmse(trainset$CHI_MLU[!is.na(trainset$CHI_MLU)], predict(modelquad))
  MSEtest[n] <- rmse(testset$CHI_MLU[!is.na(testset$CHI_MLU)], predict(modelquad, testset[!is.na(testset$CHI_MLU), ], allow.new.levels = TRUE))
  n = n+1
}

mean(MSEtrain) #0.296
mean(MSEtest) #0.551

#WE ARE AWARE THAT TAKING THE MEAN IS NOT OPTIMAL - HOWEVER, MALTE SAID THAT IN THIS CASE THE DIFFERENCE IS NOT TOO BIG, AND IT IS OKAY. WE CHANGED IT FOR FUTURE ASSIGNMENT; E.G. WHEN CALCULATING ACCURACY MEASURES
```

We compared multiple models using cross-validation. Our quadratic model from last week (child MLU ~ diagnosis * (visit + visit^2) + ADOS + verbal IQ + (visit + visit^2|subject)) produced the lowest cross validated errors on bost the training data (MSE = 0.293) and the test data (MSE = 0.581).

### Exercise 3) Assessing the single child

Let's get to business. This new kiddo - Bernie - has entered your clinic. This child has to be assessed according to his group's average and his expected development.

Bernie is one of the six kids in the test dataset, so make sure to extract that child alone for the following analysis.

You want to evaluate:

- how does the child fare in ChildMLU compared to the average TD child at each visit? Define the distance in terms of absolute difference between this Child and the average TD.
(Tip: recreate the equation of the model: Y=Intercept+BetaX1+BetaX2, etc; input the average of the TD group  for each parameter in the model as X1, X2, etc.).

- how does the child fare compared to the model predictions at Visit 6? Is the child below or above expectations? (tip: use the predict() function on Bernie's data only and compare the prediction with the actual performance of the child)

```{r, include = FALSE}
train$SUBJ <- as.character(train$SUBJ)
test2$Age <- as.numeric(test2$Age)
TrainAndTest <- full_join(train, test2)

DataWithoutBernie = filter(TrainAndTest, !(SUBJ == "Bernie"))
finalmodel <- lmer(CHI_MLU ~ Diagnosis * (VISIT + I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT+I(VISIT^2)|SUBJ), DataWithoutBernie, REML = FALSE)
summary(finalmodel)

#TYPICALLY DEVELOPING
TDsub <- subset(train, Diagnosis == "TD")
TDsub <- TDsub[-1,]

TDAdos <- mean(TDsub$ADOS1, na.rm = TRUE)
TDverbal <- mean(TDsub$verbalIQ, na.rm = TRUE)

tdpred = distinct(train, VISIT) %>%
  mutate(Diagnosis = "TD",
         ADOS1 = TDAdos,
         verbalIQ1 = TDverbal)

TDPred <- predict(finalmodel, tdpred, allow.new.levels = TRUE, re.form = NA)

#BERNIE
Bernie <- filter(test2, SUBJ == "Bernie")
#BerniePred <- predict(finalmodel, Bernie, allow.new.levels = TRUE)

Bernie[1, 12]-TDPred[1] #0.722
Bernie[2, 12]-TDPred[2] #0.689
Bernie[3, 12]-TDPred[3] #1.026
Bernie[4, 12]-TDPred[4] #0.506
Bernie[5, 12]-TDPred[5] #0.266
Bernie[6, 12]-TDPred[6] #0.433


#BERNIE REAL VERSUS PREDICTED
DataWithoutBernie6 = filter(TrainAndTest, !(SUBJ == "Bernie" & VISIT == 6))
BernieVisit6 = filter(test2, (SUBJ == "Bernie" & VISIT == 6))
BernieModel = lmer(CHI_MLU ~ Diagnosis * (VISIT + I(VISIT^2)) + ADOS1 + verbalIQ1 + (1+VISIT+I(VISIT^2)|SUBJ), DataWithoutBernie6, REML = FALSE)
BerniePred1 <- predict(BernieModel, newdata = BernieVisit6, allow.new.levels = TRUE)

Bernie[6, 12]-BerniePred1 #0.3975
```

Compared to the predicted mean length of utterance for a typically developing child (the model was trained on all data except Bernie visit one through six), the child's MLU is above the expected value at the first visit (Bernie[1]-TD[1] = 0.722). At the second visit the child is also above the expected value for an average TD child (Bernie[2]-TD[2] = 0.689). This trend continues throughout the third (Bernie[3]-TD[3] = 1.026), fourth (Bernie[4]-TD[4] = 0.506), fifth (Bernie[5]-TD[5] = 0.266) and sixth (Bernie[6]-TD[6] = 0.433) visit, as seen in Figure 7.
The child is also slightly above the predictions of the model (this model was trained on all data except Bernie at visit six) at the sixth visit, given the child's previous data (Bernie[6]-BerniePredicted[6] = 0.398), as can be observed in Figure 8.

```{r, warning = FALSE, echo = FALSE}
# plotting model predictions - MALTE
TD = filter(train, Diagnosis == "TD")
TD = TD[-1,]
# make fake data to represent the average
expand.grid(VISIT = 1:6,
            SUBJ = "TD",
            Diagnosis = "TD",
            ADOS1 = mean(TD$ADOS1),
            verbalIQ1 = mean(TD$verbalIQ1)) %>%
  # make predictions based on this fake data
  mutate(CHI_MLU = predict(finalmodel, newdata = ., allow.new.levels = TRUE)) %>%
  # plot those predictions
  ggplot(aes(VISIT, CHI_MLU, colour = "Model Average for \n Typically Developing Children \n (child MLU ~ diagnosis * (visit + visit^2) + \n ADOS + verbal IQ + (visit + visit^2|subject))")) +
  geom_line() +
  geom_point(aes(colour = "Data Points for Bernie"), data = Bernie) +
  labs(x = "Visit", y = "Child MLU", title = "Figure 7") +
  scale_colour_brewer(palette = "Dark2") +
  theme(legend.title=element_blank())

#plotting Bernie predictions
Bernie %>%
  # make predictions based on this fake data
  mutate(CHI_MLU = predict(BernieModel, newdata = ., allow.new.levels = TRUE)) %>%
  # plot those predictions
  ggplot(aes(VISIT, CHI_MLU, colour = "Model Predictions for Bernie \n (child MLU ~ diagnosis * (visit + visit^2) + \n ADOS + verbal IQ + (visit + visit^2|subject))")) +
  geom_line() +
  geom_point(aes(colour = "Data Points for Bernie"), data = Bernie) +
  labs(x = "Visit", y = "Child MLU", title = "Figure 8") +
  scale_colour_brewer(palette = "Dark2") + theme(legend.title=element_blank())
```


### OPTIONAL: Exercise 4) Model Selection via Information Criteria
Another way to reduce the bad surprises when testing a model on new data is to pay close attention to the relative information criteria between the models you are comparing. Let's learn how to do that!

Re-create a selection of possible models explaining ChildMLU (the ones you tested for exercise 2, but now trained on the full dataset and not cross-validated).

Then try to find the best possible predictive model of ChildMLU, that is, the one that produces the lowest information criterion.

- Bonus question for the optional exercise: are information criteria correlated with cross-validated RMSE? That is, if you take AIC for Model 1, Model 2 and Model 3, do they co-vary with their cross-validated RMSE?

### OPTIONAL: Exercise 5): Using Lasso for model selection

Welcome to the last secret exercise. If you have already solved the previous exercises, and still there's not enough for you, you can expand your expertise by learning about penalizations. Check out this tutorial: http://machinelearningmastery.com/penalized-regression-in-r/ and make sure to google what penalization is, with a focus on L1 and L2-norms. Then try them on your data!

